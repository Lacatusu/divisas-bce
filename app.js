'use strict'

//Extern npm dependencies
const bodyParser     = require('body-parser');
const cors           = require('cors');
const express        = require('express');

//Init app
const app            = express();
const router         = express.Router();

//Middleware body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Use routes
app.use('/', router);

//Cors to allow extern connections
app.options('*', cors()) 

//Routes to obtain the endpoint
require('./lib/currency')(router);

//Init the server
app.listen(9090, () => console.log('Listening in port 9090'));

