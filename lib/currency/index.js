'use strict'

//Import the promise who return the json
const currency  = require('./bce_parse_xml');

//Routes
module.exports = router => {
  router.get('/currency', function (req,res) {
  	  currency().then((result) => {
        res.send(result);
    })
  });
}
